# Gitlab CICD Project

## Description

The requirement was to build and deploy a simple project leveraging the concepts of CI/CD pipelines and Terraform.

### Idea

To maximize the learning opportunity, I intend to redo most of the concepts learned and decided to implement G-Autos application, a Spring Boot CRUD API with persistence to PostgreSQL in multiple ways.

## Design

The initial design was to build and deploy g-autos spring boot API application on an AWS instance using Gradle and Terrafrom Base CI templates.

  1. Build the jar file
  2. Load Jar file to an S3 bucket
  3. Create an EC2 instance and copy jar file from S3 bucket
  4. Install Java and Docker on EC2
  5. Run postgres Docker image
  6. Run jar file

Sample Output:
```json
[{"id":1,"year":"1967","make":"Ford","model":"Mustang"},{"id":2,"year":"1970","make":"AMC","model":"Gremlin"},{"id":3,"year":"1981","make":"Chrysler","model":"Imperial"},{"id":4,"year":"1972","make":"Chevrolet","model":"Impala"},{"id":5,"year":"1981","make":"DeLorean","model":"DMC-12"},{"id":6,"year":"1982","make":"Cadillac","model":"Cimarron"},{"id":7,"year":"1982","make":"Renault","model":"Fuego"},{"id":8,"year":"1982","make":"Alfa-Romeo","model":"Arna"},{"id":9,"year":"1984","make":"Ford","model":"Bronco II"},{"id":10,"year":"2022","make":"Tesla","model":"Model 3"}
```

## Other Implementations

1. To get  familiar with ECS and serverless computing, I deployed postgres docker container on AWS Fargate. 

2. Containerize the Spring app and install it on AWS instance instead of loading the jar file to S3 buckets.

3. Create a postgres database using Amazon RDS instead of usign docker container.



## Usage

### Local Machine

Follow below instructions to manually build the spring application and deploy a PostgreSQL database using docker on your local machine.

#### Requirements
- Java 11
- PostgreSQL

#### PostgreSQL

```
docker run -p 5432:5432 --name postgres -e POSTGRES_USER=autos -e  POSTGRES_PASSWORD=autos123 -d postgres
```
To connect to a remote database, edit the values in `src/main/resources/application.properties`

#### Spring Boot API

- Build
  ```
  ./gradlew build
  ```
  - Output directory: `build/libs`
- Run
  ```
  ./gradlew bootRun
  ```
- Test
  ```
  ./gradlew test

Once the application is running and connected to database, below URL shoud be accessible.

```
https://localhost:8080/autos

```
### AWS

Once the pipleline successfully builds the application and  deploys the AWS infrastructure using gradle and terraform templates. 

Application should be running and accessible through browser when connected to EC2 instance public URL on port 8080
```
http://ec2-44-200-234-91.compute-1.amazonaws.com:8080/autos

```

## Additional Functionality

 Included Jobs/SAST-IaC.latest.gitlab-ci.yml template to the pipeline to trigger Static Applciation Security Testing (SAST).

## Issues

Docker image of Spring Application when ran in local machine did not connec to postgres as expected due to route to host issues.

## TODO

1. Create RDS and ECS resources using terraform
2. Deploy both Postgres and sg-autos docker containers on AWS Fargate using terraform
