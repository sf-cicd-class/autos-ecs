terraform {
  backend "http" {
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "sg_s3_spring" {}

resource "aws_s3_object" "sg_s3_autosjar" {
  bucket = aws_s3_bucket.sg_s3_spring.id
  key    = "autos-spring-app.jar"
  source = "build/libs/g-autos-0.0.1-SNAPSHOT.jar"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("build/libs/g-autos-0.0.1-SNAPSHOT.jar")
}

resource "aws_security_group" "sg_allow_ports_8080_22" {
  #name        = "sg_allow_ssh_http"
  #description = "Allow SSH and HTTP inbound traffic"
  #vpc_id      = "vpc-089af8b9a5b06c061"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }
  ingress {
    description = "HTTP 8080"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  #tags = {
  #  Name = "allow_ssh_http"
  # }
}

resource "aws_iam_role_policy" "sg_spring_role_policy" {
  #name = "sg_spring_role_policy"
  role = aws_iam_role.sg_spring_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:Describe*",
          "s3:ListBucket",
          "s3:GetObject"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role" "sg_spring_role" {
  #name = "sg_spring_role2"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_instance_profile" "sg_spring_instance_profile" {
  #name = "sg_spring_instance_profile2"
  role = aws_iam_role.sg_spring_role.name
}

resource "aws_instance" "sg_ec2_spring" {
  ami                  = "ami-0dfcb1ef8550277af"
  instance_type        = "t2.micro"
  key_name             = "saketh-ssh-key"
  iam_instance_profile = aws_iam_instance_profile.sg_spring_instance_profile.name
  user_data            = <<EOF
                    #! /bin/bash
                    sudo yum update -y
                    sudo yum install -y java-11-amazon-corretto-headless
                    sudo yum install docker -y
                    sudo systemctl enable docker.service
                    sudo systemctl start docker.service
#                   sudo docker run -p 5432:5432 --name postgres -e POSTGRES_USER=autos -e  POSTGRES_PASSWORD=autos123 -d postgres
                    sudo docker run -p 8080:8080 --name sg-autos  gsakethreddy/sg-autos-image:latest
#                     aws s3api get-object --bucket "${aws_s3_bucket.sg_s3_spring.id}" --key "${aws_s3_object.sg_s3_autosjar.id}" "${aws_s3_object.sg_s3_autosjar.id}"
#                      java -jar ${aws_s3_object.sg_s3_autosjar.id}

                    EOF

  security_groups = ["sg_allow_ssh_http"]
}
